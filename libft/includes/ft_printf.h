/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <rnarbo@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/09 16:59:05 by orhaegar          #+#    #+#             */
/*   Updated: 2019/07/20 15:52:42 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <string.h>

typedef union
{
	unsigned long long	ll;
	long double			ld;
	void				*vp;
}						t_value;

typedef struct
{
	t_value				val;
	char				sign : 1;
}	t_sgvalue;

struct					s_lenght
{
	char				hh : 1;
	char				h : 1;
	char				ll : 1;
	char				l : 1;
	char				lb : 1;
	char				z : 1;
	char				j : 1;
	char				t : 1;
};

typedef struct
{
	char				align_left : 1;
	char				alt : 1;
	size_t				num;
	char				filling;
	char				sign;
	struct s_lenght		lenght;
	size_t				id;
	int					width;
	int					prec;
	char				type;
	t_value				value;
	char				color;
	char				bgcolor;
}						t_spec;

typedef ssize_t			(*t_conv)(t_spec *, char **);

int						ft_printf(const char *format, ...);
int						ft_dprintf(int fd, const char *format, ...);
int						ft_vdprintf(int fd, const char *format, va_list ap);
int						ft_sprintf(char *str, const char *format, ...);
int						ft_vsprintf(char *str, const char *format, va_list ap);

#endif
