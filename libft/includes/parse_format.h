/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_format.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/27 10:36:18 by rnarbo            #+#    #+#             */
/*   Updated: 2019/09/15 22:03:34 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSE_FORMAT_H
# define PARSE_FORMAT_H

# include "ft_printf.h"
# include "libft.h"

# define LENGTHS "hlLzjt"

size_t	parse_digit(t_spec *spec, char *format);
size_t	parse_flags(t_spec *spec, char *format);
size_t	parse_length(t_spec *spec, char *format);
size_t	parse_prec(t_spec *spec, char *format);
size_t	parse_color(t_spec *spec, char *format);

int		take_value(t_spec *spec, va_list args, t_sgvalue *value);

size_t	parse_format(va_list ar, t_list **spec, const char *frm);

#endif
