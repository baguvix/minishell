/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   take_value.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/09 14:10:27 by rnarbo            #+#    #+#             */
/*   Updated: 2019/07/19 10:30:23 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parse_format.h"
#include "libft.h"
#include <stddef.h>
#include <wchar.h>

static void	take_value_di(t_spec *spec, va_list args, t_sgvalue *value)
{
	long long	tmp_v;

	value->sign = 0;
	if (spec->lenght.ll && spec->type != 'D')
		tmp_v = (long long)va_arg(args, long);
	else if (spec->lenght.l || spec->type == 'D')
		tmp_v = va_arg(args, long long);
	else if (spec->lenght.z)
		tmp_v = (long long)(size_t)va_arg(args, size_t);
	else if (spec->lenght.j)
		tmp_v = (long long)(intmax_t)va_arg(args, intmax_t);
	else if (spec->lenght.hh)
		tmp_v = (long long)((char)va_arg(args, int));
	else if (spec->lenght.h)
		tmp_v = (long long)((short)va_arg(args, int));
	else if (spec->lenght.t)
		tmp_v = (long long)(ptrdiff_t)va_arg(args, ptrdiff_t);
	else
		tmp_v = (long long)(int)va_arg(args, int);
	if (tmp_v < 0)
	{
		value->sign = 1;
		tmp_v = ~tmp_v + 1;
	}
	value->val.ll = tmp_v;
}

static void	take_value_uxo(t_spec *spec, va_list args, t_sgvalue *value)
{
	unsigned long long	tmp_v;

	value->sign = 0;
	if (spec->lenght.ll && spec->type != 'U' && spec->type != 'O')
		tmp_v = va_arg(args, long long int);
	else if (spec->lenght.l || spec->type == 'U' || spec->type == 'O')
		tmp_v = (unsigned long long)va_arg(args, long);
	else if (spec->lenght.z)
		tmp_v = (unsigned long long)(size_t)va_arg(args, size_t);
	else if (spec->lenght.j)
		tmp_v = (unsigned long long)(intmax_t)va_arg(args, intmax_t);
	else if (spec->lenght.hh)
		tmp_v = (unsigned long long)((unsigned char)va_arg(args, int));
	else if (spec->lenght.h)
		tmp_v = (unsigned long long)((unsigned short)va_arg(args, int));
	else if (spec->lenght.t)
		tmp_v = (unsigned long long)(ptrdiff_t)va_arg(args, ptrdiff_t);
	else
		tmp_v = (unsigned long long)(unsigned int)va_arg(args, int);
	value->val.ll = tmp_v;
}

static void	take_value_feg(t_spec *spec, va_list args, t_sgvalue *value)
{
	value->sign = 0;
	if (spec->lenght.lb)
		value->val.ld = va_arg(args, long double);
	else
		value->val.ld = (long double)va_arg(args, double);
}

static void	take_value_c(t_spec *spec, va_list args, t_sgvalue *value)
{
	value->sign = 0;
	if (spec->lenght.l || spec->type == 'C')
		value->val.ll = (unsigned long long int)((wchar_t)va_arg(args, wint_t));
	else
		value->val.ll =
			(unsigned long long int)((unsigned char)va_arg(args, int));
}

int			take_value(t_spec *spec, va_list args, t_sgvalue *value)
{
	if (ft_strchr("dDi", spec->type))
		take_value_di(spec, args, value);
	else if (ft_strchr("uUxXoO", spec->type))
		take_value_uxo(spec, args, value);
	else if (ft_strchr("fFeEgG", spec->type))
		take_value_feg(spec, args, value);
	else if (spec->type == 'c' || spec->type == 'C')
		take_value_c(spec, args, value);
	else if (spec->type == 'p' || spec->type == 's' || spec->type == 'S')
	{
		value->sign = 0;
		value->val.vp = va_arg(args, void *);
	}
	else
	{
		value->val.vp = 0;
		return (0);
	}
	return (1);
}
