/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fp_conv_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/29 21:15:40 by orhaegar          #+#    #+#             */
/*   Updated: 2019/07/19 21:35:07 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "fp_core.h"

int		mul_2(t_bn *n)
{
	size_t				i;
	unsigned long long	carry;

	i = n->len;
	carry = 0;
	while (i--)
		if (n->p[i] || carry)
		{
			n->p[i] = (n->p[i] << 1) + carry;
			carry = n->p[i] / DIGIT_MASK;
			n->p[i] %= DIGIT_MASK;
		}
	return (0);
}

int		div_2(t_bn *n)
{
	size_t				i;
	unsigned long long	tmp;
	int					carry;

	i = 0;
	carry = 0;
	while (i < n->len)
	{
		if (n->p[i] || carry)
		{
			tmp = n->p[i];
			n->p[i] = (n->p[i] >> 1);
			n->p[i] += (carry * 5 * DIGIT_MASK / 10);
			carry = tmp % 2;
		}
		++i;
	}
	return (0);
}

void	num_map(t_bn *n, char *s, size_t str_len)
{
	size_t	i;
	size_t	j;

	j = n->len - 1;
	while (str_len)
	{
		i = 0;
		while (str_len && i++ < 18)
		{
			s[--str_len] += n->p[j] % 10;
			n->p[j] /= 10;
		}
		--j;
	}
	return ;
}

void	correction(char *s, size_t len)
{
	char	*stmp;

	if (s[len] > '5')
	{
		--len;
		while (s[len] == '9')
			s[len--] = '0';
		s[len] += 1;
		return ;
	}
	stmp = s + len + 1;
	if (s[len] == '5')
	{
		while (*stmp && *stmp == '0')
			++stmp;
		--len;
		if ((s[len] - '0' + 1) % 2 == 0 || *stmp)
		{
			while (s[len] == '9')
				s[len--] = '0';
			s[len] += 1;
		}
	}
	return ;
}

char	*e_help(t_e *e, char *s)
{
	char	*stmp;

	e->pow = 0;
	e->pow_sign = '+';
	stmp = s;
	while (*++stmp != '.' && *stmp)
		++e->pow;
	if (e->pow == 0 && (stmp)[-1] == '0')
	{
		++e->pow;
		++stmp;
		e->pow_sign = '-';
		while (*stmp && *stmp++ == '0')
			++e->pow;
		if (*stmp == 0)
		{
			e->pow_sign = '+';
			e->pow = 0;
		}
	}
	return (e->pow_sign == '+' ? s : stmp - 1);
}
