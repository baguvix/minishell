/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   choose_conv.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <rnarbo@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/10 18:16:34 by rnarbo            #+#    #+#             */
/*   Updated: 2019/07/21 22:10:16 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "conv.h"
#include "libft.h"

static ssize_t	percent_conv(t_spec *spec, char **rtu)
{
	ssize_t	ret;
	char	*res;

	ret = spec->width;
	ret = ret > 0 ? ret : 1;
	if ((res = (char *)malloc(ret)) == 0)
		return (-1);
	if (spec->align_left == 0)
	{
		ft_memset(res, spec->filling, ret);
		res[ret - 1] = '%';
	}
	else
	{
		ft_memset(res, ' ', ret);
		res[0] = '%';
	}
	*rtu = res;
	return (ret);
}

static ssize_t	wrong_conv(t_spec *spec, char **rtu)
{
	size_t len;

	if (!spec || !rtu)
		return (-1);
	len = spec->width > 1 ? spec->width : 1;
	if ((*rtu = malloc(len * sizeof(char))) == 0)
		return (-1);
	ft_memset(*rtu, spec->filling, len);
	if (spec->align_left)
		(*rtu)[0] = spec->type;
	else
		(*rtu)[len - 1] = spec->type;
	return (len);
}

static ssize_t	empty_conv(t_spec *spec, char **rtu)
{
	spec += 0;
	*rtu = ft_strdup("");
	return (0);
}

t_conv			choose_conv(t_spec *spec)
{
	if (spec->type != 0 && ft_strchr("fFeE", spec->type))
		return (&fe_conv);
	if (spec->type == 'c' || spec->type == 'C')
		return (&c_conv);
	if (spec->type == 'S' || (spec->type == 's' && spec->lenght.l))
		return (&bs_conv);
	if (spec->type == 's' || spec->type == '0')
		return (&s_conv);
	if (spec->type == '%')
		return (&percent_conv);
	if (spec->type != 0 && ft_strchr("dDiuU", spec->type))
		return (&diu_conv);
	if (spec->type == 'o' || spec->type == 'O')
		return (&o_conv);
	if (spec->type == 'x' || spec->type == 'X')
		return (&x_conv);
	if (spec->type == 'p')
		return (&p_conv);
	if (spec->type == '\0')
		return (&empty_conv);
	return (&wrong_conv);
}
