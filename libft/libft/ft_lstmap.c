/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:40:24 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:40:30 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*fresh;

	if (!(lst && f))
		return (NULL);
	if (!(fresh = f(lst)))
		return (NULL);
	if (lst->next)
		if (!(fresh->next = ft_lstmap(lst->next, f)))
		{
			free(fresh);
			fresh = NULL;
		}
	return (fresh);
}
