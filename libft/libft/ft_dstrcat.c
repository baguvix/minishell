/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dstrcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/17 17:35:59 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/17 19:08:58 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_dstrcat(char *s1, char *s2)
{
	size_t	len1;
	size_t	len2;
	char	*str;

	len1 = 0;
	len2 = 0;
	if (s1)
		len1 = ft_strlen(s1);
	if (s2)
		len2 = ft_strlen(s2);
	if ((str = (char *)malloc(len1 + len2 + 1)) == NULL)
		return (NULL);
	if (s1)
	{
		ft_memcpy(str, s1, len1);
		free(s1);
	}
	if (s2)
		ft_memcpy(str + len1, s2, len2 + 1);
	return (str);
}
