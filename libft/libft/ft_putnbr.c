/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:47:30 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:47:33 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr(int n)
{
	long	tmp;
	size_t	pos;
	char	c;

	pos = 1;
	tmp = n;
	if (tmp < 0)
	{
		write(1, "-", 1);
		tmp *= -1;
	}
	while (n /= 10)
		pos *= 10;
	while (pos)
	{
		c = (tmp / pos) % 10 + '0';
		write(1, &c, 1);
		pos /= 10;
	}
	return ;
}
