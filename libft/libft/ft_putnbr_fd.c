/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:48:29 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:48:30 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	long	tmp;
	size_t	pos;
	char	c;

	pos = 1;
	tmp = n;
	if (tmp < 0)
	{
		write(fd, "-", 1);
		tmp *= -1;
	}
	while (n /= 10)
		pos *= 10;
	while (pos)
	{
		c = (tmp / pos) % 10 + '0';
		write(fd, &c, 1);
		pos /= 10;
	}
	return ;
}
