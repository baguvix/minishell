/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <rnarbo@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/12 17:56:29 by rnarbo            #+#    #+#             */
/*   Updated: 2019/07/11 15:32:03 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstsort(t_list *head, int (*cmp)(t_list *, t_list *))
{
	t_list	*tmp;
	void	*swap;
	int		flag;

	if (!head || !cmp)
		return ;
	flag = 1;
	while (flag)
	{
		tmp = head;
		flag = 0;
		while (tmp->next)
		{
			if (cmp(tmp, tmp->next) > 0)
			{
				swap = tmp->content;
				tmp->content = tmp->next->content;
				tmp->next->content = swap;
				flag = 1;
			}
			tmp = tmp->next;
		}
	}
}
