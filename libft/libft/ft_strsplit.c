/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 15:58:56 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/14 20:59:21 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	w_count(char *where, char sym)
{
	size_t	count;

	count = 0;
	if (!where)
		return (count);
	while (*where)
		if (*where++ != sym)
		{
			++count;
			while (*where && *where != sym)
				++where;
		}
	return (count);
}

static char		**wipe(char **mas, size_t n)
{
	while (n--)
		free(mas[n]);
	free(mas);
	return (NULL);
}

char			**ft_strsplit(char const *s, char c)
{
	size_t	i;
	size_t	n;
	char	**mas;
	char	*tmp;

	n = w_count((char *)s, c);
	if ((mas = (char **)malloc((n + 1) * sizeof(char *))) == NULL)
		return (NULL);
	i = 0;
	while (n && *s)
	{
		while (*s == c)
			++s;
		tmp = (char *)s;
		while (*tmp && *tmp != c)
			++tmp;
		if ((tmp - s) == 0)
			break ;
		if ((mas[i] = ft_strsub(s, 0, tmp - s)) == NULL)
			return (wipe(mas, i));
		s = tmp;
		++i;
	}
	mas[i] = NULL;
	return (mas);
}
