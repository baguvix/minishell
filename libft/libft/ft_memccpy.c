/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:43:13 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:43:16 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned char	a;
	char			*c_dst;
	char			*c_src;

	c_dst = (char*)(dst);
	c_src = (char*)(src);
	a = (unsigned char)(c);
	while (n--)
		if ((unsigned char)(*c_dst++ = *c_src++) == a)
			return ((void*)c_dst);
	return (NULL);
}
