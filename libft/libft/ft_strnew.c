/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:53:22 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:53:26 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	char	*mem;
	char	*tmp;

	if (!(mem = (char*)malloc(++size * sizeof(char))))
		return (NULL);
	tmp = mem;
	while (size--)
		*tmp++ = '\0';
	return (mem);
}
