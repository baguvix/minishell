/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_exit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/14 22:34:44 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:42:21 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin_exit.h"

int		builtin_exit(int argc, char **argv, struct s_env *e)
{
	(void)argc;
	(void)argv;
	free_2arr((void ***)&e->var, 0);
	free_2arr((void ***)&e->default_var, 0);
	exit(0);
}
