/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_env.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/25 22:24:10 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:54:42 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin_env.h"

void	parse_env_args(int ac, char **av, struct s_env_env *e)
{
	int	c;

	while ((c = ft_getopt(ac, av, ENV_FLAGS_SPEC)) != -1)
		if (c == 'P')
		{
			set_bit(&e->flags, getopt_id(c, ENV_FLAGS), 1);
			e->altpath = g_optarg;
		}
		else
			break ;
	if (c == '?' || c == ':')
	{
		if (c == '?')
			ft_dprintf(STDERR, "env: illegal option -- %c\n", g_optopt);
		else
			ft_dprintf(STDERR,
					"env: option requires an argument -- %c\n", g_optopt);
		exit(EXIT_FAILURE);
	}
}

int		builtin_env(int argc, char **argv, struct s_env *e)
{
	struct s_env_env	e_e;
	int					i;

	parse_env_args(argc, argv, &e_e);
	if (g_optind < argc)
	{
		ft_dprintf(STDERR, "env: too many arguments -- %s\n", argv[g_optind]);
		return (1);
	}
	else
	{
		i = 0;
		while (e->var[i])
			ft_printf("%s\n", e->var[i++]);
	}
	return (0);
}
