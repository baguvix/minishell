/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_funcs.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 14:56:59 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:43:38 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "generic.h"

void	free_2arr(void ***arr, int i)
{
	char	***a;

	a = (char ***)(arr);
	if (!i)
		while ((*a)[i++])
			;
	while (i)
		free((*a)[--i]);
	free(*a);
	*a = NULL;
}

void	clear_env(struct s_env *e)
{
	free_2arr((void ***)&e->var, 0);
	free_2arr((void ***)&e->default_var, 0);
}
