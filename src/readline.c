/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readline.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 15:59:17 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/18 01:26:12 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "readline.h"

int		lf_handle(char c, char *buf, ssize_t len)
{
	if (c == '\n')
		buf[len] = '\0';
	else if (!len)
		ft_printf("%s\n", ft_strcpy(buf, "exit"));
	else
		return (1);
	return (0);
}

int		readline(char *buf)
{
	ssize_t	rc;
	ssize_t	len;
	char	c;

	len = 0;
	rc = 1;
	while (rc)
	{
		if ((rc = read(STDIN, &c, 1)) < 0)
		{
			if (!g_stop)
				return (1);
			buf[0] = '\0';
			return (g_stop = 0);
		}
		if (!rc || c == '\n')
			if ((rc = lf_handle(c, buf, len)))
				continue ;
		if (rc && len < BUF_SIZE - 1)
			buf[len++] = (c == '\t') ? ' ' : c;
	}
	return (0);
}
