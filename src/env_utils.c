/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 20:38:47 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:43:10 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "env_utils.h"

char	*assemble_envvar(char *key, char *value)
{
	int		len;
	char	*var;

	len = ft_strlen(key);
	len += ft_strlen(value);
	++len;
	if ((var = (char *)malloc((len + 1) * sizeof(char))) == NULL)
		return (NULL);
	ft_strcpy(var, key);
	ft_strcat(var, "=");
	ft_strcat(var, value);
	var[len] = '\0';
	return (var);
}

int		append_var(char *key, char *value, struct s_env *e)
{
	int		len;
	char	**new_env;

	len = 0;
	while (e->var[len])
		++len;
	++len;
	if ((new_env = (char **)malloc((len + 1) * sizeof(char *))) == NULL
		|| (new_env[len - 1] = assemble_envvar(key, value)) == NULL)
		return (1);
	ft_memcpy(new_env, e->var, sizeof(char *) * (len - 1));
	new_env[len] = NULL;
	free(e->var);
	e->var = new_env;
	return (0);
}

int		find_entry(char **env, char *key)
{
	int		i;
	int		keylen;
	char	*end;
	char	c;

	i = 0;
	c = 0;
	end = key;
	while (*end && (ft_isalnum(*end) || *end == '_' || *end < 0))
		++end;
	if (*end)
	{
		c = *end;
		*end = '\0';
	}
	keylen = ft_strlen(key);
	if (c)
		*end = c;
	while (env[i])
	{
		if (!ft_strncmp(env[i], key, keylen) && env[i][keylen] == '=')
			return (i);
		++i;
	}
	return (-1);
}
