/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_cd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/16 18:01:07 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 23:57:23 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin_cd.h"

void	gen_canon(char **path, char **old)
{
	int		i;
	int		j;

	i = -1;
	j = 0;
	while (old[++i])
	{
		path[j++] = old[i];
		if (!ft_strcmp(old[i], "..") && ft_strlen(old[i]) == 2)
			j -= (j > 1) ? 2 : 1;
		if (old[i][0] == '.' && old[i][1] == '\0')
			--j;
	}
	path[j] = NULL;
}

char	*convert_to_canon(char *arg, char *av)
{
	char	**mas;
	char	**tmp;
	char	*path;
	int		len;

	if ((mas = ft_strsplit(arg, '/')) == NULL)
		return (NULL);
	len = count_args(mas);
	if ((tmp = (char **)malloc((len + 1) * sizeof(char *))) == NULL)
	{
		free_2arr((void ***)&mas, len);
		return (NULL);
	}
	gen_canon(tmp, mas);
	path = assemble_path(tmp, av);
	free_2arr((void ***)&mas, 0);
	free(tmp);
	return (path);
}

char	*get_path(int ac, char **av, struct s_env *e)
{
	char	*dir;
	char	*tmp;
	int		id;

	if (ac == 1 || !ft_strcmp(av[1], "-"))
	{
		if ((id = find_entry(e->var, ac == 1 ? "HOME" : "OLDPWD")) < 0
			|| !*(dir = ft_strchr(e->var[id], '=') + 1))
		{
			ft_printf("bosh: cd: %s not set\n", ac == 1 ? "HOME" : "OLDPWD");
			return (NULL);
		}
		return (ft_strdup(dir));
	}
	if (*av[1] != '/')
	{
		if ((tmp = getcwd(NULL, 0)) == NULL
			|| (tmp = ft_dstrcat(tmp, "/")) == NULL
			|| (tmp = ft_dstrcat(tmp, av[1])) == NULL)
			return (NULL);
		dir = convert_to_canon(tmp, av[1]);
		free(tmp);
		return (dir);
	}
	return (convert_to_canon(av[1], av[1]));
}

int		update(char *dir, char *cwd, struct s_env *e)
{
	char	*in_av[4];
	int		rc;

	in_av[3] = NULL;
	in_av[1] = "PWD";
	in_av[2] = dir;
	rc = builtin_setenv(3, in_av, e);
	in_av[1] = "OLDPWD";
	in_av[2] = cwd;
	rc = rc ? rc : builtin_setenv(3, in_av, e);
	return (rc);
}

int		builtin_cd(int argc, char **argv, struct s_env *e)
{
	char	*dir;
	char	*cwd;
	int		rc;

	dir = get_path(argc, argv, e);
	cwd = getcwd(NULL, 0);
	rc = 1;
	if (dir && cwd)
	{
		rc = chdir(dir);
		if (!rc)
			rc = update(dir, cwd, e);
		if (!rc && argc > 1 && argv[1][0] == '-' && argv[1][1] == '\0')
			ft_printf("%s\n", dir);
	}
	free(cwd);
	free(dir);
	return (rc);
}
