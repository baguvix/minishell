/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getopt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 20:40:57 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 20:50:02 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "getopt.h"

int		getopt_id(char c, char *options)
{
	int i;

	i = 0;
	while (options[i] && options[i] != c)
		++i;
	return (options[i] ? i : -1);
}

void	set_bit(t_opts *opts, int id, int value)
{
	if (value)
		*opts |= (1ULL << id);
	else
		*opts &= ~(1ULL << id);
}

int		ft_getopt(int ac, char **av, char *options)
{
	static int	i;
	char		*opt;

	if (g_optind >= ac || (av[g_optind][0] != '-') || av[g_optind][1] == '\0'
				|| (av[g_optind][1] == '-' && ++g_optind))
		return (-1);
	++i;
	if ((opt = ft_strchr(options, av[g_optind][i])) == NULL)
	{
		g_optopt = (int)(av[g_optind][i]);
		return ('?');
	}
	if (av[g_optind][i + 1] == '\0' && ++g_optind)
		i = 0;
	if (opt[1] == ':')
	{
		if (i)
		{
			g_optopt = (int)(*opt);
			return (':');
		}
		g_optarg = av[g_optind++];
	}
	return ((int)(*opt));
}
