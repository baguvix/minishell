/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 13:33:19 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:44:43 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	busy_loop(struct s_env *e)
{
	char		buf[BUF_SIZE];

	while (1)
	{
		ft_printf("bush$> ");
		if (readline(buf))
		{
			clear_env(e);
			exit(EXIT_FAILURE);
		}
		if (interpret_cmd(buf, e))
		{
			clear_env(e);
			exit(EXIT_FAILURE);
		}
	}
}

void	cather(int sig)
{
	(void)sig;
	g_stop = write(1, "\n", 1);
}

int		main(int argc, char **argv, char **environ)
{
	struct s_env	env;

	(void)argc;
	(void)argv;
	if (init_env(&env, environ))
		exit(EXIT_FAILURE);
	if (signal(SIGINT, cather) == SIG_ERR || siginterrupt(SIGINT, 1) < 0)
		exit(EXIT_FAILURE);
	busy_loop(&env);
	return (0);
}
