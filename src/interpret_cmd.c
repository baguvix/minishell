/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmdexe.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 18:47:39 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:44:23 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "interpret_cmd.h"

int		tilde_expansion(char **args, struct s_env *e)
{
	int		i;
	int		id;
	char	*tmp;

	i = -1;
	while (args[++i])
		if (args[i][0] == '~')
		{
			if (!args[i][1] || args[i][1] == ' ' || args[i][1] == '/')
			{
				if ((id = find_entry(e->var, "HOME")) < 0)
					tmp = "=";
				else
					tmp = e->var[id];
			}
			else
				continue ;
			if ((tmp = ft_strdup(ft_strchr(tmp, '=') + 1)) == NULL
				|| (tmp = ft_dstrcat(tmp, args[i] + 1)) == NULL)
				return (1);
			free(args[i]);
			args[i] = tmp;
		}
	return (0);
}

int		parameter_expansion(char **args, struct s_env *e)
{
	int		i;
	int		id;
	int		keylen;
	char	*tmp;
	char	*key;

	i = -1;
	while (args[++i])
		while ((tmp = ft_strchr(args[i], '$')) != NULL)
		{
			if (!*(tmp + 1))
				break ;
			id = find_entry(e->var,
					(key = tmp + 1));
			keylen = key_processing(id, &key, e);
			if ((tmp = ft_strsub(args[i], 0, tmp - args[i])) == NULL
				|| (tmp = ft_dstrcat(tmp, key)) == NULL
				|| (tmp = ft_dstrcat(tmp,
						ft_strchr(args[i], '$') + 1 + keylen)) == NULL)
				return (1);
			free(args[i]);
			args[i] = tmp;
		}
	return (0);
}

char	**parse_cmd(char *cmd, struct s_env *e)
{
	char	**args;
	char	**rc;

	rc = NULL;
	args = ft_strsplit(cmd, ' ');
	if (args && *args != NULL)
		if (!tilde_expansion(args, e))
			if (!parameter_expansion(args, e))
				rc = args;
	if (!rc)
		free_2arr((void ***)&args, 0);
	return (rc);
}

int		(*classify_cmd(char *cmd))(int, char **, struct s_env *)
{
	if (!ft_strcmp(cmd, "env"))
		return (builtin_env);
	if (!ft_strcmp(cmd, "setenv"))
		return (builtin_setenv);
	if (!ft_strcmp(cmd, "unsetenv"))
		return (builtin_unsetenv);
	if (!ft_strcmp(cmd, "exit"))
		return (builtin_exit);
	if (!ft_strcmp(cmd, "echo"))
		return (builtin_echo);
	if (!ft_strcmp(cmd, "cd"))
		return (builtin_cd);
	return (ft_system);
}

int		interpret_cmd(char *cmd, struct s_env *e)
{
	char	**argv;
	int		argc;
	int		(*call)(int, char **, struct s_env *);
	int		status;

	argv = parse_cmd(cmd, e);
	if (argv == NULL)
		return (0);
	argc = count_args(argv);
	call = classify_cmd(argv[0]);
	status = call(argc, argv, e);
	free_2arr((void ***)&argv, argc);
	return (0);
}
