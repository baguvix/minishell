/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 14:58:15 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 23:46:39 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "free_funcs.h"
#include "utils.h"

int		key_processing(int id, char **key, struct s_env *e)
{
	int		keylen;

	keylen = 0;
	while (**key && (ft_isalnum(**key) || **key == '_' || **key < 0))
	{
		++*key;
		++keylen;
	}
	if (id < 0)
		*key = "";
	else
		*key = ft_strchr(e->var[id], '=') + 1;
	return (keylen);
}

char	**dup_2arr(char **arr)
{
	char	**new_arr;
	int		len;
	int		i;

	len = 0;
	while (arr[len])
		++len;
	new_arr = (char **)malloc((len + 1) * sizeof(char *));
	if (new_arr == NULL)
		return (NULL);
	i = 0;
	new_arr[len] = NULL;
	while (new_arr && i < len)
	{
		if ((new_arr[i] = ft_strdup(arr[i])) == NULL)
			free_2arr((void ***)&new_arr, i);
		++i;
	}
	return (new_arr);
}

int		count_args(char **argv)
{
	int	res;

	res = 0;
	while (argv[res])
		++res;
	return (res);
}

int		validate_path(char *path, char *av)
{
	struct stat	st;
	int			status;

	status = 0;
	if (!status && stat(path, &st) < 0)
		status = 1;
	if (!status && !(st.st_mode & S_IFDIR))
		status = ft_dprintf(2, "bush: cd: %s: Not a directory\n", av);
	if (!status && access(path, X_OK) < 0)
		status = ft_dprintf(2, "bush: cd: %s: Permission denied\n", av);
	if (access(path, F_OK) < 0)
		status =
	ft_dprintf(2, "bush: cd: %s: No such file or directory\n", av);
	return (status);
}

char	*assemble_path(char **mas, char *av)
{
	char	*path;

	path = NULL;
	if (*mas == NULL)
		path = ft_dstrcat(path, "/");
	while (*mas)
	{
		if ((path = ft_dstrcat(path, "/")) == NULL
			|| (path = ft_dstrcat(path, *mas)) == NULL
			|| validate_path(path, av) != 0)
		{
			free(path);
			return (NULL);
		}
		++mas;
	}
	return (path);
}
