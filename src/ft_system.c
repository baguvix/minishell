/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_system.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 20:39:55 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 23:45:46 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_system.h"

int		search_dir(char *dir, char *name)
{
	DIR				*dirp;
	struct dirent	*dp;
	size_t			len;

	dirp = opendir(dir);
	if (dirp == NULL)
		return (-1);
	len = ft_strlen(name);
	while ((dp = readdir(dirp)) != NULL)
		if (dp->d_namlen == len && !ft_strcmp(dp->d_name, name))
		{
			closedir(dirp);
			return (1);
		}
	closedir(dirp);
	return (0);
}

char	*create_path(char *dir, char *name)
{
	char	*path;
	char	*tmp;

	path = ft_strdup(dir);
	if (path == NULL)
		return (NULL);
	tmp = ft_dstrcat(path, "/");
	if (tmp == NULL)
	{
		free(path);
		return (NULL);
	}
	path = tmp;
	tmp = ft_dstrcat(path, name);
	if (tmp == NULL)
	{
		free(path);
		return (NULL);
	}
	return (tmp);
}

int		full_path(char *name, char **envp, char **path)
{
	int		id;
	char	**dirs;

	*path = NULL;
	id = find_entry(envp, "PATH");
	if (id < 0)
		return (0);
	dirs = ft_strsplit(ft_strchr(envp[id], '=') + 1, ':');
	if (!dirs)
		return (-1);
	id = 0;
	while (dirs[id])
		if (search_dir(dirs[id], name) > 0)
		{
			if ((*path = create_path(dirs[id], name)) == NULL)
				id = -1;
			break ;
		}
		else
			++id;
	free_2arr((void ***)&dirs, 0);
	return (id > -1 ? 0 : -1);
}

int		isrunnable(char *path, char *cmd)
{
	struct stat	st;
	int			status;

	status = 0;
	if (access(path, F_OK) < 0)
		status = path == NULL ?
			ft_dprintf(2, "bush: %s: command not found\n", cmd) :
			ft_dprintf(2, "bush: %s: No such file or directory\n", path);
	if (!status && access(path, X_OK) < 0)
		status = ft_dprintf(2, "bush: %s: Permission denied\n", path);
	if (!status && stat(path, &st) < 0)
		status = -1;
	if (!status && (st.st_mode & S_IFDIR))
		status = ft_dprintf(2, "bush: %s: is a directory\n", path);
	return (status);
}

int		ft_system(int argc, char **argv, struct s_env *e)
{
	char		*path;
	int			rc;
	pid_t		pid;

	(void)argc;
	if (ft_strchr(argv[0], '/') == NULL)
	{
		if (full_path(argv[0], e->var, &path) < 0)
			return (-1);
	}
	else
		path = ft_strdup(argv[0]);
	if ((rc = isrunnable(path, argv[0])) == 0)
	{
		pid = fork();
		if (!pid)
			execve(path, argv, e->var);
		if (waitpid(pid, &rc, 0) != pid)
			if (!g_stop)
				rc = ft_dprintf(2, "Critical runtime error\n") * -1;
	}
	free(path);
	return (rc);
}
