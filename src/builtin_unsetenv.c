/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_unsetenv.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 20:38:28 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:42:43 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin_unsetenv.h"

void	rearrange(int i, char **e)
{
	free(e[i]);
	while (e[i++])
		e[i - 1] = e[i];
}

int		builtin_unsetenv(int argc, char **argv, struct s_env *e)
{
	int		id;

	if (argc != 2)
	{
		if (argc > 2)
			ft_dprintf(STDERR, "unsetenv: too many arguments\n");
		ft_dprintf(STDERR, "usage:\n\tunsetenv VAR_NAME\n");
		return (1);
	}
	id = find_entry(e->var, argv[1]);
	if (id > 0)
		rearrange(id, e->var);
	return (0);
}
