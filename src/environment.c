/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   environment.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 13:44:03 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:43:29 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "environment.h"

int		init_env(struct s_env *env, char **e)
{
	if ((env->var = dup_2arr(e)) == NULL)
		return (1);
	if ((env->default_var = dup_2arr(e)) == NULL)
		return (1);
	return (0);
}
