/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_echo.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/16 17:05:59 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:41:48 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin_echo.h"

int		builtin_echo(int argc, char **argv, struct s_env *e)
{
	int	i;

	(void)(e);
	i = 1;
	while (i < argc)
	{
		ft_printf("%s", argv[i]);
		if (i < argc - 1 && ft_strlen(argv[i]) > 0)
			ft_printf(" ");
		++i;
	}
	ft_printf("\n");
	return (0);
}
