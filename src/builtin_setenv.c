/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_setenv.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 20:34:22 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:42:34 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin_setenv.h"

int		builtin_setenv(int argc, char **argv, struct s_env *e)
{
	int		id;

	if (argc != 3)
	{
		if (argc > 3)
			ft_dprintf(STDERR, "setenv: too many arguments\n");
		else
			ft_dprintf(STDERR, "setenv: not enough arguments\n");
		ft_dprintf(STDERR, "usage:\n\tsetenv VAR_NAME VAR_VALUE\n");
		return (1);
	}
	id = find_entry(e->var, argv[1]);
	if (id >= 0)
	{
		free(e->var[id]);
		if ((e->var[id] = assemble_envvar(argv[1], argv[2])) == NULL)
			return (1);
	}
	else if (append_var(argv[1], argv[2], e))
		return (1);
	return (0);
}
