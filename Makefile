NAME = minishell

SRCDIR = src
OBJDIR = objects
LIBDIR = libft
LIBINC = $(LIBDIR)/includes
INCLUDE = -I include -I $(LIBINC)
LIB = ft
CFLAGS = -Wall -Werror -Wextra $(INCLUDE)
CC = gcc

SRCFILES = main.c \
		   environment.c \
		   free_funcs.c \
		   readline.c \
		   env_utils.c \
		   ft_system.c \
		   getopt.c	\
		   interpret_cmd.c \
		   utils.c \
		   builtin_env.c \
		   builtin_setenv.c	\
		   builtin_unsetenv.c \
		   builtin_exit.c \
		   builtin_echo.c \
		   builtin_cd.c

OBJFILES = $(SRCFILES:.c=.o)

all: $(LIB) $(NAME)

$(NAME): $(OBJDIR) $(addprefix $(OBJDIR)/, $(OBJFILES))
	$(CC) $(CFLAGS) $(addprefix $(OBJDIR)/, $(OBJFILES)) -L $(LIBDIR) -l$(LIB) -o $@

$(OBJDIR):
	mkdir $@

$(LIB):
	make -C $(LIBDIR)

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	make clean -C $(LIBDIR)
	rm -rf $(OBJDIR)

fclean: clean
	make fclean -C $(LIBDIR)
	rm -f $(NAME)

re: fclean all
