/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 15:34:39 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/17 23:55:41 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include "readline.h"
# include "environment.h"
# include "interpret_cmd.h"
# include "internal_getopt.h"
# include "ft_system.h"
# include "free_funcs.h"

int		g_stop = 0;

#endif
