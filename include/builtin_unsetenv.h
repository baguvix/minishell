/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_unsetenv.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 22:23:15 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:39:19 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUILTIN_UNSETENV_H
# define BUILTIN_UNSETENV_H

# include "generic.h"
# include "env_utils.h"

int		builtin_unsetenv(int argc, char **argv, struct s_env *e);

#endif
