/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 22:27:08 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:41:16 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H

# include "generic.h"

char	**dup_2arr(char **arr);
int		count_args(char **args);
int		validate_path(char *path, char *av);
char	*assemble_path(char **mas, char *av);
int		key_processing(int id, char **key, struct s_env *e);

#endif
