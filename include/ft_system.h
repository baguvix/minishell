/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_system.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 13:50:18 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:40:17 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SYSTEM_H
# define FT_SYSTEM_H

# include "generic.h"
# include "builtin_env.h"
# include "env_utils.h"
# include "free_funcs.h"

extern int	g_stop;

int			ft_system(int argc, char **argv, struct s_env *e);

#endif
