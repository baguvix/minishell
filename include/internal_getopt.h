/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   internal_getopt.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/17 23:55:20 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/17 23:55:22 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INTERNAL_GETOPT_H
# define INTERNAL_GETOPT_H

# include "getopt.h"

int		g_optopt;
int		g_optind = 1;
char	*g_optarg;

#endif
