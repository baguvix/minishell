/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_funcs.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 19:01:49 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:40:08 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FREE_FUNCS_H
# define FREE_FUNCS_H

# include "generic.h"

void	free_2arr(void ***arr, int i);
void	clear_env(struct s_env *e);

#endif
