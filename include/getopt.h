/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getopt.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 22:26:21 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:26:52 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GETOPT_H
# define GETOPT_H

# include "libft.h"

# define ISSET(x, y)	((x) & (1ULL << (y)))

typedef unsigned long long	t_opts;

extern int					g_optopt;
extern int					g_optind;
extern char					*g_optarg;

int							getopt_id(char c, char *options);
void						set_bit(t_opts *opts, int id, int value);
int							ft_getopt(int ac, char **av, char *options);

#endif
