/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_cd.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/17 17:38:40 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:38:26 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUILTIN_CD_H
# define BUILTIN_CD_H

# include "generic.h"
# include "builtin_setenv.h"
# include "free_funcs.h"
# include "env_utils.h"
# include "utils.h"

int		builtin_cd(int argc, char **argv, struct s_env *e);

#endif
