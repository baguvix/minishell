/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_env.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 22:22:00 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:38:50 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUILTIN_ENV_H
# define BUILTIN_ENV_H

# include "generic.h"
# include "getopt.h"
# include "builtin_setenv.h"

# define ENV_FLAGS		"P"
# define ENV_FLAGS_SPEC	"P:"

enum		e_env_flags
{
	P
};

struct		s_env_env
{
	t_opts	flags;
	char	*altpath;
};

int			builtin_env(int argc, char **argv, struct s_env *e);

#endif
