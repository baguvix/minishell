/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_utils.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 22:24:38 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:39:40 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUILTIN_ENV_UTILS_H
# define BUILTIN_ENV_UTILS_H

# include "generic.h"

char	*assemble_envvar(char *key, char *value);
int		append_var(char *key, char *value, struct s_env *e);
int		find_entry(char **env, char *key);

#endif
