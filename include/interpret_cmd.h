/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmdexe.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 18:56:09 by orhaegar          #+#    #+#             */
/*   Updated: 2019/11/19 22:40:57 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INTERPRET_CMD_H
# define INTERPRET_CMD_H

# include "utils.h"
# include "generic.h"
# include "env_utils.h"
# include "builtin_env.h"
# include "builtin_setenv.h"
# include "builtin_unsetenv.h"
# include "builtin_exit.h"
# include "builtin_echo.h"
# include "builtin_cd.h"
# include "ft_system.h"

int		interpret_cmd(char *cmd, struct s_env *e);

#endif
